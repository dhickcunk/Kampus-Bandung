package info.dhickcunk.tugasapi_143040087;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    private RecyclerView kampusList;
    LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setTitle("Kampus Bandung");

        kampusList = (RecyclerView) findViewById(R.id.kampusList);
        linearLayoutManager = new LinearLayoutManager(MainActivity.this);
        kampusList.setLayoutManager(linearLayoutManager);
        new GetKampusAsyncTask().execute();
    }

    private class GetKampusAsyncTask extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute () {
            super.onPreExecute();
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground (String...params){
            String respon = "";
            try {
                String url = "http://sab.if-unpas.org/tugas_07/kampus.php?action=get_kampus";
                respon = CustomHttpClient.executeHttpGet(url);
            } catch (Exception e) {
                respon = e.toString();
            }
            return respon;
        }

        @Override
        protected void onPostExecute (String result){
            super.onPostExecute(result);
            progressDialog.dismiss();
            try {
                Log.e("masuk", "RESPON result -> " + result);
                JSONObject object = new JSONObject(result);
                ArrayList<HashMap<String, String>> arr = new ArrayList<>();
                if (object.getString("success").equalsIgnoreCase("1")) {
                    JSONArray array = object.getJSONArray("data");
                    HashMap<String, String> map;
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject jsonObject = array.getJSONObject(i);
                        map = new HashMap<String, String>();
                        map.put("logo", jsonObject.getString("logo"));
                        map.put("nama", jsonObject.getString("nama"));
                        map.put("telepon", jsonObject.getString("telepon"));
                        map.put("alamat", jsonObject.getString("alamat"));
                        map.put("deskripsi", jsonObject.getString("deskripsi"));
                        arr.add(map);
                    }
                }
                kampusList.setAdapter(new KampusAdapter(arr, MainActivity.this));
            } catch (Exception e) {
                Log.e("masuk", "-> " + e.getMessage());
            }
        }
    }
}
